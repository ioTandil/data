import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  items: FirebaseListObservable<any[]>;
  isDataAvailable:boolean = false;

  public lineChartData:Array<any> = [
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Temperatura (ºC)', yAxisID: 'temp_axis', fill:false},
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Humedad (%)', yAxisID: 'hum_axis', fill:false}
    ];
    
  public lineChartLabels:Array<any> = [1,2,3,4,5,6,7];

  public lineChartType:string = 'line';
  public lineChartOptions:any = {
    responsive: true,
    scales: {
      yAxes: [
        {
          id: 'temp_axis',
          position: 'left',
          ticks: {
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: 'ºC'
          }
        },
        {
          id: 'hum_axis',
          position: 'right',
          ticks: {
            beginAtZero: true,
            max: 100
          },
          scaleLabel: {
            display: true,
            labelString: '%'
          }
        }
      ]
    }
  };

  constructor(af: AngularFire) {
    this.items = af.database.list('/items',
      {
        query: {
          limitToLast: 30
        }
      }
    );
  }

  ngOnInit() {
    this.items.subscribe(data => {
      let _lineChartDataHumedad:Array<any> = new Array(data.length);
      let _lineChartDataTemperatura:Array<any> = new Array(data.length);
      let _lineChartLabels:Array<any> = new Array(data.length);
      for (let i in data)
      {
          _lineChartDataHumedad[i] = data[i]["humedad"];
          _lineChartDataTemperatura[i] = data[i]["temperatura"];
          var date_diff = Math.round((new Date()).getTime() / 1000) - data[i]["hora"];
          _lineChartLabels[i] = Math.floor(date_diff / 3600)  
                        + ":" + Math.floor(date_diff / 60) % 60 
                        + ":" + date_diff % 60;
      }
      this.lineChartData[0]["data"] = _lineChartDataTemperatura;
      this.lineChartData[1]["data"] = _lineChartDataHumedad;
      this.lineChartLabels = _lineChartLabels;
      this.isDataAvailable = true;
    });
  }

  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
}
