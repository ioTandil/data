import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { ChartsModule } from 'ng2-charts/ng2-charts';

export const firebaseConfig = {
  apiKey: "AIzaSyD3mTssfL5G-3OWxZe8s6ifN2L_U_e5dNg",
  authDomain: "data-c528d.firebaseapp.com",
  databaseURL: "https://data-c528d.firebaseio.com",
  projectId: "data-c528d",
  storageBucket: "data-c528d.appspot.com",
  messagingSenderId: "847639613899"
};


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    FormsModule,
    HttpModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
